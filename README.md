# future-template-web

#### 介绍
future-template web端

#### 软件架构
vue.js ant-design-vue ui axios vuecli

#### 安装教程

1.  git clone https://gitee.com/jbqc/future-template-web.git
2.  npm install

#### 使用说明

1.  运行：npm run serve
2.  打包：npm run build
3.  预览：http://www.jiubanqingchen.com:8688    demo 123456
