const getters = {
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  userName: state => state.user.userName,
  userId: state => state.user.userId,
  menus: state => state.user.menus,
  collapsed: state => state.setting.collapsed,
  menuTheme: state => state.setting.menuTheme,
  menuType: state => state.setting.menuType,
  fixSidebar: state => state.setting.fixSidebar,
  fixHeader: state => state.setting.fixHeader,
  device: state => state.setting.device
}
export default getters
