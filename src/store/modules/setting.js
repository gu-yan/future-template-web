/**
 * 全局配置管理
 * light_dust 2020/4/24
 */
import Vue from 'vue'
import {
    COLLAPSED,
    DEFAULT_MENU_THEME,
    DEFAULT_MENU_TYPE,
    DEFAULT_FIXED_HEADER,
    DEFAULT_FIXED_SIDEMENU
} from '@/config/storageKey'
const state = {
    collapsed: false,//是否收缩 （默认不收缩）
    menuTheme: 'dark',//菜单主题
    menuType: 'sideMenu',//菜单栏类型 sideMenu topMenu
    fixSidebar: true,//固定侧边栏 默认固定(这样体验会稍微好一点)
    fixHeader: true,//固定顶部内容 默认固定(这样体验会稍微好一点)
    device: 'desktop'//终端类型 //判断为 pc ipad mobile 默认电脑端
}

const mutations = {
    SET_COLLAPSED: (state, collapsed) => {
        Vue.ls.set(COLLAPSED, collapsed);
        state.collapsed = collapsed
    },
    SET_MENU_THEME: (state, menuTheme) => {
        Vue.ls.set(DEFAULT_MENU_THEME, menuTheme);
        state.menuTheme = menuTheme
    },
    SET_MENU_TYPE: (state, menuType) => {
        Vue.ls.set(DEFAULT_MENU_TYPE, menuType);
        state.menuType = menuType
    },
    SET_FIX_SIDEBAR: (state, fixSidebar) => {
        Vue.ls.set(DEFAULT_FIXED_SIDEMENU, fixSidebar);
        state.fixSidebar = fixSidebar
    },
    SET_FIX_HEADER: (state, fixHeader) => {
        Vue.ls.set(DEFAULT_FIXED_HEADER, fixHeader);
        state.fixHeader = fixHeader
    },
    SET_DEVICE: (state, device) => {
        state.device = device
    }
}
const actions = {
    toggleCollapsed({ commit }, collapsed) {
        commit('SET_COLLAPSED', collapsed)
    },
    toggleMenuTheme({ commit }, menuTheme) {
        commit('SET_MENU_THEME', menuTheme)
    },
    toggleMenuType({ commit }, menuType) {
        commit('SET_MENU_TYPE', menuType)
    },
    toggleFixSidebar({ commit }, fixSidebar) {
        commit('SET_FIX_SIDEBAR', fixSidebar)
    },
    toggleFixHeader({ commit }, fixHeader) {
        commit('SET_FIX_HEADER', fixHeader)
    },
    toggleDevice({ commit }, device) {
        commit('SET_DEVICE', device)
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
