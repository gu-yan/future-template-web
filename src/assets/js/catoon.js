/*
 * @Description:白屏页面加载动画
 * @Autor: Zhu_liangyu
 * @Date: 2020-07-02 20:05:49
 */

var eleStr = '<div class="preloadLoading">'
    + "<div class='loading'>"
    + "<div class='loading__square'></div>"
    + "<div class='loading__square'></div>"
    + "<div class='loading__square'></div>"
    + "<div class='loading__square'></div>"
    + "<div class='loading__square'></div>"
    + "<div class='loading__square'></div>"
    + "<div class='loading__square'></div>"
    + "</div>"
    + "</div>";
document.getElementById('app').innerHTML = eleStr
