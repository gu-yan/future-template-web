/*
 * @Description: 
 * @Autor: Zhu_liangyu
 * @Date: 2020-07-14 17:19:56
 */
import { notification } from 'ant-design-vue'
const WebSocketUtil = {};

/**
 * @description 处理不同的方法
 * @author light_dust
 */
WebSocketUtil.handleMessage = function (message) {
    var data = JSON.parse(message);
    var code = data.code;
    switch (code) {
        case 30000:
            notification.success({
                message: '成功',
                description: data.msg
            });
            console.log("准备处理通知");
            break;
        case 30001:
            notification.success({
                message: '成功',
                description: data.msg
            });
            console.log("准备处理消息");
            break
        case 30002:
            notification.success({
                message: '成功',
                description: data.msg
            });
            console.log("准备处理代办");
        case 30003:
            notification.warning({
                message: '成功',
                description: data.msg,
                btn: h => {
                    return h(
                        'a-button',
                        {
                            props: {
                                type: 'primary',
                                size: 'small',
                            },
                            on: {
                                click: () => notification.close(key),
                            },
                        },
                        'Confirm',
                    );
                }
            });
            break
    }
}

export default WebSocketUtil