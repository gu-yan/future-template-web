/*
 * @Description: 
 * @Autor: Zhu_liangyu
 * @Date: 2020-05-09 10:52:56
 */
/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
    return /^(https?:|mailto:|tel:)/.test(path)
}

export function judgeUploadImage(file) {
    const isJpgOrPng =
        file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
        this.$message.error("只能上传JPG文件!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        this.$message.error("文件大小不能超过2MB!");
    }
    return isJpgOrPng && isLt2M;
}

export function validUserPassword(password) {
    const pattern = /^[\w_-]{6,16}$/;//密码在6-16位之间 
    return pattern.test(password);
}