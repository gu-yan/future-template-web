
/**
 * @author zhu_liangyu
 * @date 2019/9/16
 * @description 基础工具类
 */

/**
 * 首字母转小写
 * @param {*} str 
 */
export function firstCharToUpperCase(str) {
    if (isNotBank(str)) {
        return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
    }
    return null;
}

/**
 * 下划线转驼峰 dictionary_type ==> dictionaryType
 * @param {*} str 
 * @returns new_str 
 */
export function underlineToHump(str) {
    if (str.indexOf("_") == -1) {
        return str.toLowerCase();
    }
    var strs = str.split('_');
    var new_str = '';
    for (var i = 0; i < strs.length; i++) {
        if (i == 0) {
            new_str = strs[i].toLowerCase();
        } else {
            new_str += firstCharToUpperCase(strs[i]);
        }
    }
    return new_str;
}

/**
 * 判断字符串是否非空,且不为空字符串
 * @param {*} str 
 */
export function isBank(str) {
    return str !== null && str !== undefined && str != '';
}

/**
 * 判断字符串是否为空,空字符串
 * @param {*} str 
 */
export function isNotBank(str) {
    return str !== null && str !== undefined && str != '';
}
/**
 * 全局替换字符串中某个特殊字符
 * @param {String} str 
 * @param {String} char 
 */
export function replaceAll(str, char, nweChar) {
    if (str.indexOf(char) != -1) {
        str = str.replace(char, nweChar);
        str = replaceAll(str, char, nweChar);
    }
    return str;
}

/**
 * 将对象转换为url参数
 * @param {Object} data  对象
 * @param {Boolean} flag  是否将第一位连接符由&转换为?
 */
export function objectToUrlParams(data, flag) {
    var str = "";
    if (data && data instanceof Object) {
        for (var i in data) {
            str += "&" + i + "=" + data[i];
        }
        if (flag) str = "?" + str.substring(1);
        return str;
    }
    return str;
}

/**
 * 高亮显示json对象
 * @param {Object} json 
 */
export function syntaxHighlight(json) {
    json = json
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;");
    return json.replace(
        /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
        function (match) {
            var cls = "json_number";
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = "json_key";
                } else {
                    cls = "json_string";
                }
            } else if (/true|false/.test(match)) {
                cls = "json_boolean";
            } else if (/null/.test(match)) {
                cls = "json_null";
            }
            return '<span class="' + cls + '">' + match + "</span>";
        }
    );
}