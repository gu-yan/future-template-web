import { mapState } from 'vuex'
import { deviceEnquire, DEVICE_TYPE } from '@/utils/device'

const mixin = {
    computed: {
        ...mapState({
            menuTheme: state => state.setting.menuTheme,
            menuType: state => state.setting.menuType,
            fixSidebar: state => state.setting.fixSidebar,
            fixHeader: state => state.setting.fixHeader
        })
    }
}

const mixinDevice = {
    computed: {
        ...mapState({
            device: state => state.setting.device
        })
    },
    methods: {
        isMobile() {
            return this.device === DEVICE_TYPE.MOBILE
        },
        isDesktop() {
            return this.device === DEVICE_TYPE.DESKTOP
        },
        isTablet() {
            return this.device === DEVICE_TYPE.TABLET
        }
    }
}

const AppDeviceEnquire = {
    mounted() {
        const { $store } = this
        deviceEnquire(deviceType => {
            switch (deviceType) {
                case DEVICE_TYPE.DESKTOP:
                    $store.commit('setting/SET_DEVICE', 'desktop')
                    $store.dispatch('setting/toggleCollapsed', false)
                    break
                case DEVICE_TYPE.TABLET:
                    $store.commit('setting/SET_DEVICE', 'tablet')
                    $store.dispatch('setting/toggleCollapsed', false)
                    break
                case DEVICE_TYPE.MOBILE:
                    $store.commit('setting/SET_DEVICE', 'mobile')
                    $store.dispatch('setting/toggleCollapsed', false)
                    break
            }
        })
    }
}

export { mixin, mixinDevice, AppDeviceEnquire }
