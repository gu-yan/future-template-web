/**
 * @description: 
 * @param {config} 
 * @return: 
 * @author: Zhu_liangyu
 * @Date: 2020-07-20 10:36:25
 */
export function downloadFile(config) {
    const blob = config.blob;
    const elink = document.createElement("a");
    elink.download = config.fileName;
    elink.style.display = "none";
    elink.href = URL.createObjectURL(blob);
    document.body.appendChild(elink);
    elink.click();
    URL.revokeObjectURL(elink.href); // 释放URL 对象
    document.body.removeChild(elink);
}
