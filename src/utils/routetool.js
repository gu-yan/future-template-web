import { BasicLayout, RouteView, PageView } from '@/layouts'
// 遍历后台传来的路由字符串，转换为组件对象
export function filterAsyncRouter(asyncRouterMap) {
    const accessedRouters = asyncRouterMap.filter(route => {
        if (route.component) {
            if (route.component === 'BasicLayout') {
                route.component = BasicLayout
            } else if (route.component === 'RouteView') {
                route.component = RouteView
            } else if (route.component === 'PageView') {
                route.component = PageView
            } else {
                route.component = require('@/views' + route.component).default // 导入组件
            }
            route.name = route.menuName;
            if (!route.meta) route.meta = {};
            if (route.children && route.children.length) {
                route.children = filterAsyncRouter(route.children)
                route.meta.hideAllChildren = judgeHideAllChildren(route.children);
            }
            return true
        }
    })
    return accessedRouters
}

/**
 * 判断子集是否全部隐藏
 * @param {*} children 
 */
function judgeHideAllChildren(children) {
    let hideChildren = children.filter(child => {
        return child.hidden === true;
    });
    return hideChildren.length == children.length;
}