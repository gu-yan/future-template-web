/*
 * @Description:
 * @Autor: Zhu_liangyu
 * @Date: 2020-06-13 14:34:48
 */

/**
 * 默认toolbar
 */
export const toolbar = 'undo redo removeformat | formatselect | fontsizeselect bold italic underline strikethrough | forecolor backcolor | bullist numlist ' +
    'blockquote codesample | link unlink image table | alignjustify alignleft aligncenter alignright outdent indent | code preview fullscreen';

/**
 * 默认plugins
 */
export const plugins = "link lists image code codesample table wordcount preview fullscreen contextmenu";

export const fullPlugin = 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks' +
    ' visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists' +
    ' checklist wordcount tinymcespellchecker a11ychecker textpattern noneditable help formatpainter pageembed charmap mentions quickbars linkchecker emoticons'
    + ' advtable';


