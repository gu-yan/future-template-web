import Vue from 'vue'
import store from '@/store/'
import {
    ACCESS_TOKEN,
    COLLAPSED,
    DEFAULT_MENU_THEME,
    DEFAULT_MENU_TYPE,
    DEFAULT_FIXED_HEADER,
    DEFAULT_FIXED_SIDEMENU
} from '@/config/storageKey'
import config from '@/config/defaultSettings'

export default function Initializer() {
    store.commit('setting/SET_COLLAPSED', Vue.ls.get(COLLAPSED, false))
    store.commit('setting/SET_MENU_THEME', Vue.ls.get(DEFAULT_MENU_THEME, config.menuTheme))
    store.commit('setting/SET_MENU_TYPE', Vue.ls.get(DEFAULT_MENU_TYPE, config.menuType))
    store.commit('setting/SET_FIX_SIDEBAR', Vue.ls.get(DEFAULT_FIXED_HEADER, config.fixHeader))
    store.commit('setting/SET_FIX_HEADER', Vue.ls.get(DEFAULT_FIXED_SIDEMENU, config.fixSiderbar))
    store.commit('user/SET_TOKEN', Vue.ls.get(ACCESS_TOKEN))
}