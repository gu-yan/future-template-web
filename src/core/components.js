/*
 * @Description: 
 * @Autor: Zhu_liangyu
 * @Date: 2020-05-09 10:52:56
 */
import { PageView } from '@/layouts';
import PageTable from "@/components/PageTable";
import IconFont from "@/components/IconFont";
import IconSelect from "@/components/IconSelect";
import DictionarySelect from "@/components/DictionarySelect";
import BasicForm from "@/components/Form/BasicForm";
import QueryForm from "@/components/Form/QueryForm";
import SelectUserTable from "@/components/business/SelectUserTable";
import QcLink from "@/components/QcLink";
import QcUpload from "@/components/QcUpload";
import AuthorityTool from "@/components/AuthorityTool"





export default (Vue) => {
    Vue.component('page-view', PageView);//含header页面组件 by light_dust
    Vue.component('icon-font', IconFont);//引入iconFont图标 by light_dust
    Vue.component('icon-select', IconSelect);//引入iconFont图标 by light_dust
    Vue.component('page-table', PageTable);//分页表格组件 by light_dust
    Vue.component('basic-form', BasicForm);//通用表单组件 by light_dust
    Vue.component('query-form', QueryForm);//搜索表单组件 by light_dust
    Vue.component('select-user-table', SelectUserTable);//用户选择组件by light_dust
    Vue.component('dictionary-select', DictionarySelect);//数据字典组件by light_dust
    Vue.component('qc-link', QcLink);//link(模仿el-link)by light_dust
    Vue.component('qc-upload', QcUpload);//上传组件 by light_dust
    Vue.component('authority-tool', AuthorityTool);//权限校验组件
}