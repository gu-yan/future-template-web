import Vue from 'vue'
import components from '@/core/components.js'//全局组件注册
import * as filters from '@/core/filters'//全局过滤器配置
import VueStorage from 'vue-ls';
import config from '@/config/defaultSettings'

Vue.use(components);

Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
})

Vue.use(VueStorage, config.storageOptions);