//全局过滤器
import moment from "moment";
/**
 * 得到日期的moment对象
 * @param {*} date 
 */
const momentDate = function (date) {
    return date ? moment(date) : undefined;
}

/**
 * 格式化日期字符串
 * @param {*} date 日期
 * @param {*} formatPatten 格式化patten
 */
const formatDateString = function (date, formatPatten) {
    return moment(date).format(formatPatten);
}
export { momentDate, formatDateString }