/*
 * @Description: cronTask配置文件(列表显示字段 列表搜索 表单必填校验规则 )
 * @Autor: Zhu_liangyu
 * @Date: 2020-07-01 11:51:38
 */
/** cronTask table columns */
export const cronTaskTableColumns = [
    { title: "任务编号", dataIndex: "cronTaskNo", defaultSortOrder: "ascend", sorter: true, width: "120px" },
    { title: "任务名称", dataIndex: "cronTaskName", scopedSlots: { customRender: "customCronTaskName" } },
    { title: "状态", dataIndex: "active", scopedSlots: { customRender: "customActive" }, width: "100px", align: "center" },
    { title: "表达式", dataIndex: "cronExpression" },
    { title: "执行类", dataIndex: "className" },
    { title: "任务详情", dataIndex: "description" },
    { title: "操作", scopedSlots: { customRender: "customOperation" }, width: "140px", align: "center" }
]

/** cronTask table query columns */
export const cronTaskQueryColumns = []

/** cronTask from rules */
export const cronTaskRule = {
    cronTaskName: [{ required: true, trigger: "blur", message: "请输入任务名称" }],
    cronTaskNo: [{ required: true, trigger: "blur", message: "请输入任务编号" }, { type: "number", trigger: "change", message: "任务编号只能是数字" }],
    cronExpression: [{ required: true, trigger: "blur", message: "请输入任务表达式" }],
    className: [{ required: true, trigger: "blur", message: "请输入任务执行类" }]
}