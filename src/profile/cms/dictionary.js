/*
 * @Description: 
 * @Autor: Zhu_liangyu
 * @Date: 2020-07-03 14:17:05
 */
export const dictionaryRule = {
    codeId: [{ required: true, trigger: "blur", message: "请填写字典项标识" }],
    codeName: [{ required: true, trigger: "blur", message: "请填写字典型名称" }],
    sortNo: [{ type: "number", trigger: "change", message: "排序只能是数字" }]
}

export const dictionaryTableColumns = [
    { title: "排序", dataIndex: "sortNo", defaultSortOrder: "ascend", sorter: true },
    { title: "字典项标识", dataIndex: "codeId", scopedSlots: { customRender: "customCodeId" } },
    { title: "字典项名称", dataIndex: "codeName" }
]