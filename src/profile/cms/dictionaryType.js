/*
 * @Description: 数据字典类型配置文件
 * @Autor: Zhu_liangyu
 * @Date: 2020-07-03 14:17:08
 */
export const dictionaryTypeRule = {
    codeItemId: [{ required: true, trigger: "blur", message: "请填写类型标识" }],
    codeItemName: [{ required: true, trigger: "blur", message: "请填写类型名称" }],
    sortNo: [{ type: "number", trigger: "change", message: "排序只能是数字" }]
}

export const dictionaryTypeTableColumns = [
    { title: "排序", dataIndex: "sortNo", defaultSortOrder: "ascend", sorter: true },
    { title: "类型标识", dataIndex: "codeItemId", sorter: true, scopedSlots: { customRender: "customCodeItemId" } },
    { title: "类型名称", dataIndex: "codeItemName" },
    { title: "能否更新", dataIndex: "isUpdate", scopedSlots: { customRender: "customIsUpdate" } }
]

export const dictionaryTypeQueryColumns = [
    { type: "input", name: "codeItemIdLike", label: "类型标识" },
    { type: "input", name: "codeItemName", label: "类型名称" }
]