/*
 * @Description: 
 * @Autor: Zhu_liangyu
 * @Date: 2020-07-05 17:54:32
 */
export const anonymousTableColumns = [
        { title: "路径", dataIndex: "url", scopedSlots: { customRender: "customUrl" }, width: '500px' },
        { title: "创建人", dataIndex: "createUserName" },
        { title: "创建时间", dataIndex: "createTime" },
        { title: "更新人", dataIndex: "updateUserName" },
        { title: "更新时间", dataIndex: "updateTime" },
]

export const anonymousQueryColumns = [
        { label: "路径", name: "url", type: "input" },
        { label: "创建人", name: "createUser", type: "input" },
]

export const anonymousRules = {
        url: [{ required: true, trigger: "blur", message: "请填写路径" }],
};