/*
 * @Description: 角色配置文件
 * @Autor: Zhu_liangyu
 * @Date: 2020-07-03 15:30:43
 */
export const roleRule = {
    roleName: [{ required: true, trigger: "blur", message: "请填写角色名称" }],
    roleDescription: [{ required: true, trigger: "blur", message: "请填写角色描述" }]
}


export const roleTableColumns = [
    {
        title: "角色名称", dataIndex: "roleName", sorter: true, scopedSlots: { customRender: "customRoleName" }
    },
    { title: "角色描述", dataIndex: "roleDescription" },
    {
        title: "能否修改", dataIndex: "isUpdate", scopedSlots: { customRender: "customIsUpdate" }
    },
    { title: "创建时间", dataIndex: "createTime", sorter: true }
]
