/*
 * @Description: 
 * @Autor: Zhu_liangyu
 * @Date: 2020-05-09 10:52:56
 */
export const userRule = {
    userName: [{ required: true, message: "请输入姓名", trigger: "blur" }, { min: 2, max: 6, message: "姓名必须为2-6位字符", trigger: "blur" }],
    userPhone: [{ required: true, message: "请输入电话号码", trigger: "blur" }],
    userAccount: [{ required: true, message: "请输入账号", trigger: "blur" }],
    userBrithday: [{ required: true, message: "请选择生日", trigger: "change" }],
    userEmail: [{ type: "email", required: true, message: "请输入正确的邮箱账号", trigger: "blur" }],
    userSex: [{ required: true, message: "请选择性别", trigger: "change" }]
}

export const userTableColumns = [
    { title: "用户账号", dataIndex: "userAccount", sorter: true, scopedSlots: { customRender: "customUserAccount" } },
    { title: "用户姓名", dataIndex: "userName" },
    { title: "用户电话", dataIndex: "userPhone" },
    { title: "用户邮箱", dataIndex: "userEmail" },
    { title: "用户性别", dataIndex: "userSexDesc", align: "center", scopedSlots: { customRender: "customUserSexDesc" } },
    { title: "用户生日", dataIndex: "userBrithday", scopedSlots: { customRender: "customUserBrithday" }, hidden: true }
]

export const userQueryColumns = [
    { type: "input", name: "userAccount", label: "用户账号" },
    { type: "input", name: "userName", label: "用户姓名" },
    { type: "input", name: "userPhone", label: "用户电话" },
    { type: "custom", label: "用户邮箱", scopedSlots: { customRender: "customUserEmailQuery" } },
    { type: "dictionarySelect", name: "userSex", codeItemId: "ORG_USER_SEX", label: "用户性别" },
    { type: "date", name: "userBrithday", label: "用户生日" }
]