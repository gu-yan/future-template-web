/*
 * @Description: 
 * @Autor: Zhu_liangyu
 * @Date: 2020-07-13 16:57:52
 */
export const exceptionLogTableColumns = [
    { title: "登录名称", dataIndex: "loginUserName", scopedSlots: { customRender: "customLoginUserName" } },
    {
        title: "登录ip", dataIndex: "loginIp"
    },
    {
        title: "登录地址", dataIndex: "loginArea"
    },
    {
        title: "请求地址", dataIndex: "requestUrl"
    },
    {
        title: "请求方法", dataIndex: "requestMethod"
    },
    {
        title: "异常信息", dataIndex: "exceptionMessage", ellipsis: true
    },
    {
        title: "浏览器", dataIndex: "browser"
    },
    {
        title: "操作系统", dataIndex: "os"
    },
]

export const exceptionLogQueryColumns = [
    {
        label: "登录名称",
        name: "loginUserName",
        type: "input"
    },
    {
        label: "登录ip",
        name: "loginIp",
        type: "input"
    },
    {
        label: "请求地址",
        name: "requestUrl",
        type: "input"
    }
]

export const exceptionLogRules = {
};