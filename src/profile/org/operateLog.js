/*
 * @Description: 
 * @Autor: Zhu_liangyu
 * @Date: 2020-07-13 16:59:46
 */
export const operateLogTableColumns = [
    {
        title: "登录名称", dataIndex: "loginUserName"
        , scopedSlots: { customRender: "customLoginUserName" }
    },
    {
        title: "登录ip", dataIndex: "loginIp"
    },
    {
        title: "登录地址", dataIndex: "loginArea"
    },
    {
        title: "请求地址", dataIndex: "requestUrl"
    },
    {
        title: "请求方法", dataIndex: "requestMethod"
    },
    {
        title: "浏览器", dataIndex: "browser"
    },
    {
        title: "操作系统", dataIndex: "os"
    },
    {
        title: "登录日期", dataIndex: "createTime"
    },
]

export const operateLogQueryColumns = [
    {
        label: "登录名称",
        name: "loginUserName",
        type: "input"
    },
    {
        label: "登录ip",
        name: "loginIp",
        type: "input"
    },
    {
        label: "请求地址",
        name: "requestUrl",
        type: "input"
    }
]

export const operateLogRules = {
};