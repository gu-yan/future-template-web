/*
 * @Description:菜单配置文件
 * @Autor: Zhu_liangyu
 * @Date: 2020-07-03 15:22:07
 */
const title = [{ required: true, trigger: "blur", message: "请填写菜单title" }];
const sortNo = [{ required: true, trigger: "blur", message: "请填写菜单排序" }, { type: "number", trigger: "blur", message: "排序只能是数字" }];

//菜单类型为资源时的必填项
export const buttonRule = {
    title,
    sortNo
}

//菜单类型为页面菜单时的必填项
export const menuRule = {
    title,
    sortNo,
    menuName: [{ required: true, trigger: "blur", message: "请填写菜单Name" }],
    path: [{ required: true, trigger: "blur", message: "请填写菜单Path" }],
    component: [{ required: true, trigger: "blur", message: "请填写组件地址" }]
}

export const menuTableColumns = [
    { title: "菜单名称", dataIndex: "title", scopedSlots: { customRender: "customTitle" } },
    { title: "组件地址", dataIndex: "component" },
    { title: "path", dataIndex: "path" },
    { title: "name", dataIndex: "menuName" },
    { title: "权限标识", dataIndex: "authorityKey" },
    { title: "跳转地址", dataIndex: "redirect" },
    { title: "菜单排序", dataIndex: "sortNo" },
    { title: "操作", width: "200px", scopedSlots: { customRender: "customOperation" } }
]


