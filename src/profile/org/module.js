/*
 * @Description: 组织模块配置文件
 * @Autor: Zhu_liangyu
 * @Date: 2020-07-03 15:27:24
 */
export const moduleRule = {
    moduleName: [{ required: true, trigger: "blur", message: "请填写模块名称" }],
    path: [{ required: true, trigger: "blur", message: "请填写模块路径" }],
    package: [{ required: true, trigger: "blur", message: "请填写模块包名" }],
    sortNo: [{ type: "number", trigger: "blur", message: "排序只能是数字" }]
}

export const moduleTableColumns = [
    { title: "模块排序", dataIndex: "sortNo", width: "100px", align: "center" },
    { title: "模块名称", dataIndex: "moduleName", scopedSlots: { customRender: "customModuleName" } },
    { title: "模块路径", dataIndex: "path" },
    { title: "模块包名", dataIndex: "package" },
    { title: "模块描述", dataIndex: "description" }
]