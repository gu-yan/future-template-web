/*
 * @Description: 部门配置文件
 * @Autor: Zhu_liangyu
 * @Date: 2020-05-09 10:52:56
 */
export const unitRule = {
    unitCode: [{ required: true, trigger: "blur", message: "请填写部门编码" }],
    unitName: [{ required: true, trigger: "blur", message: "请填写部门名称" }],
    unitFullName: [{ required: true, trigger: "blur", message: "请填写部门全称" }],
    unitType: [{ required: true, trigger: "change", message: "请选择部门类型" }],
    sortNo: [{ type: "number", trigger: "blur", message: "排序只能是数字" }]
}