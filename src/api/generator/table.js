import request from '@/utils/request';

export const generatorListUrl = "/cms/generator/list"
export const generatorFormUrl = "/cms/generator/form"

const tableService = "/generator/table/"

export const table_queryPageApi = tableService + "page";
export const table_queryListColumnApi = tableService + "listColumn";




/**
*@description 查询分页
*@param queryParam
*@author light_dust_creator
*@date 2019-12-16 12:02:29
*/
export function Page(queryParam) {
    return request.get(table_queryPageApi, { params: queryParam });
}

/**
 * 创建文件
 * @param {} queryParam 
 */
export function createFile(queryParam) {
    return request.post('/generator/create', queryParam);
}