/*
 * @Description: 
 * @Autor: Zhu_liangyu
 * @Date: 2020-05-31 15:30:40
 */
import request from '@/utils/request'
import URL_KEY from '@/api/urlKeys'

const loginLogService = "/org/loginLog/";//用户service

export const loginLogListUrl = "/org/logManager/list";
export const loginLogFormUrl = "/org/logManager/form";

export const loginLog_queryPageApi = loginLogService + URL_KEY.page;
export const loginLog_queryListApi = loginLogService + URL_KEY.list;
export const loginLog_queryByIdApi = loginLogService + URL_KEY.id;
export const loginLog_insertApi = loginLogService + URL_KEY.add;
export const loginLog_updateApi = loginLogService + URL_KEY.update;
export const loginLog_deleteApi = loginLogService + URL_KEY.delete;
export const loginLog_deletesApi = loginLogService + URL_KEY.deletes;

/**
*@description 新增
*@param loginLog
*@author light_dust_creator
*@date 2020-05-31 16:55:36
*/
export function loginLogInsert(loginLog) {
	return request.post(loginLog_insertApi, loginLog);
}

/**
*@description 删除
*@param loginLog
*@author light_dust_creator
*@date 2020-05-31 16:55:36
*/
export function loginLogDelete(loginLog) {
	return request.post(loginLog_deleteApi, loginLog);
}

/**
*@description 删除_批量
*@param loginLog
*@author light_dust_creator
*@date 2020-05-31 16:55:36
*/
export function loginLogDeletes(loginLogs) {
	return request.post(loginLog_deletesApi, loginLogs);
}

/**
*@description 修改
*@param loginLog
*@author light_dust_creator
*@date 2020-05-31 16:55:36
*/
export function loginLogUpdate(loginLog) {
	return request.post(loginLog_updateApi, loginLog);
}

/**
*@description 查询分页
*@param queryParam
*@author light_dust_creator
*@date 2020-05-31 16:55:36
*/
export function loginLogPage(queryParam) {
	return request.get(loginLog_queryPageApi, { params: queryParam });
}

/**
*@description 查询所有
*@param queryParam
*@author light_dust_creator
*@date 2020-05-31 16:55:36
*/
export function loginLogList(queryParam) {
	return request.get(loginLog_queryListApi, { params: queryParam });
}

/**
*@description 查询详情
*@param loginLogId id
*@author light_dust_creator
*@date 2020-05-31 16:55:36
*/
export function getLoginLog(loginLogId) {
	return request.get(loginLog_queryByIdApi + loginLogId);
}