import request from '@/utils/request'
import { objectToUrlParams } from '@/utils/tool-str'
import URL_KEY from '@/api/urlKeys'

const userDeptService = "/org/userDept/";//用户部门service

export const userDept_queryPageApi = userDeptService + URL_KEY.page;//用户部门分页查询
export const userDept_queryListApi = userDeptService + URL_KEY.list;//用户部门全部查询
export const userDept_queryByIdApi = userDeptService + URL_KEY.id;//用户部门id查询
export const userDept_insertApi = userDeptService + URL_KEY.add;//用户部门新增
export const userDept_insertBatchApi = userDeptService + URL_KEY.addBatch;//用户部门批量新增
export const userDept_updateApi = userDeptService + URL_KEY.update;//用户部门修改
export const userDept_deleteApi = userDeptService + URL_KEY.delete;//用户部门删除
export const userDept_deletesApi = userDeptService + URL_KEY.deletes;//用户部门批量删除

/**
*@description 新增
*@param userDept
*@author light_dust_creator
*@date 2019-12-27 17:20:16
*/
export function userDeptInsert(userDept) {
    return request.post(userDept_insertApi, userDept);
}

export function userDeptInsertBatch(userDepts) {
    return request.post(userDept_insertBatchApi, userDepts);
}

/**
*@description 删除
*@param userDept
*@author light_dust_creator
*@date 2019-12-27 17:20:16
*/
export function userDeptDelete(userDept) {
    return request.post(userDept_deleteApi, userDept);
}

/**
*@description 删除_批量
*@param userDept
*@author light_dust_creator
*@date 2019-12-27 17:20:16
*/
export function userDeptDeletes(userDept) {
    return request.post(userDept_deletesApi, userDept);
}

/**
*@description 修改
*@param userDept
*@author light_dust_creator
*@date 2019-12-27 17:20:16
*/
export function userDeptUpdate(userDept, extData) {
    var url = userDept_updateApi;
    url += objectToUrlParams(extData, true);
    return request.post(url, userDept);
}

/**
*@description 查询分页
*@param queryParam
*@author light_dust_creator
*@date 2019-12-27 17:20:16
*/
export function userDeptPage(queryParam) {
    return request.get(userDept_queryPageApi, { params: queryParam });
}

/**
*@description 查询所有
*@param queryParam
*@author light_dust_creator
*@date 2019-12-27 17:20:16
*/
export function userDeptList(queryParam) {
    return request.get(userDept_queryListApi, { params: queryParam });
}

/**
*@description 查询详情
*@param userDeptId id
*@author light_dust_creator
*@date 2019-12-27 17:20:16
*/
export function getUserDept(userDeptId) {
    return request.get(userDept_queryByIdApi + userDeptId);
}