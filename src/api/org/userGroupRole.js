import request from '@/utils/request'
import URL_KEY from '@/api/urlKeys'

const userGroupRoleService = "/org/userGroupRole/";//用户,部门角色关联service

export const userGroupRole_queryPageApi = userGroupRoleService + URL_KEY.page;//用户部门分页查询
export const userGroupRole_queryListApi = userGroupRoleService + URL_KEY.list;//用户部门全部查询
export const userGroupRole_queryByIdApi = userGroupRoleService + URL_KEY.id;//用户部门id查询
export const userGroupRole_insertApi = userGroupRoleService + URL_KEY.add;//用户部门新增
export const userGroupRole_insertBatchApi = userGroupRoleService + URL_KEY.addBatch;//用户部门批量新增
export const userGroupRole_updateApi = userGroupRoleService + URL_KEY.update;//用户部门修改
export const userGroupRole_deleteApi = userGroupRoleService + URL_KEY.delete;//用户部门删除
export const userGroupRole_deletesApi = userGroupRoleService + URL_KEY.deletes;//用户部门批量删除

/**
*@description 新增
*@param userGroupRole
*@author light_dust_creator
*@date 2020-01-03 09:48:23
*/
export function userGroupRoleInsert(userGroupRole) {
	return request.post(userGroupRole_insertApi, userGroupRole);
}

/**
*@description 新增
*@param userGroupRole
*@author light_dust_creator
*@date 2020-01-03 09:48:23
*/
export function userGroupRoleInsertBatch(userGroupRoles) {
	return request.post(userGroupRole_insertBatchApi, userGroupRoles);
}

/**
*@description 删除
*@param userGroupRole
*@author light_dust_creator
*@date 2020-01-03 09:48:23
*/
export function userGroupRoleDelete(userGroupRole) {
	return request.post(userGroupRole_deleteApi, userGroupRole);
}

/**
*@description 删除_批量
*@param userGroupRole
*@author light_dust_creator
*@date 2020-01-03 09:48:23
*/
export function userGroupRoleDeletes(userGroupRole) {
	return request.post(userGroupRole_deletesApi, userGroupRole);
}

/**
*@description 修改
*@param userGroupRole
*@author light_dust_creator
*@date 2020-01-03 09:48:23
*/
export function userGroupRoleUpdate(userGroupRole) {
	return request.post(userGroupRole_updateApi, userGroupRole);
}

/**
*@description 查询分页
*@param queryParam
*@author light_dust_creator
*@date 2020-01-03 09:48:23
*/
export function userGroupRolePage(queryParam) {
	return request.get(userGroupRole_queryPageApi, { params: queryParam });
}

/**
*@description 查询所有
*@param queryParam
*@author light_dust_creator
*@date 2020-01-03 09:48:23
*/
export function userGroupRoleList(queryParam) {
	return request.get(userGroupRole_queryListApi, { params: queryParam });
}

/**
*@description 查询详情
*@param userGroupRoleId id
*@author light_dust_creator
*@date 2020-01-03 09:48:23
*/
export function getUserGroupRole(userGroupRoleId) {
	return request.get(userGroupRole_queryByIdApi + userGroupRoleId);
}