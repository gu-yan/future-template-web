import request from '@/utils/request'
/**
 * 校验用户是否有此权限
 * @param {String} authorityKey 
 */
export function judgeAuthority(authorityKey) {
    var params = { "authorityKey": authorityKey };
    return request.get("/org/manager/judgeAuthority", { params: params });
}


/**
 * 校验用户是否有一组权限
 * @param {Array} authorityKeys 
 */
export function judgeAuthorityArray(authorityKeys) {
    var params = { "authorityKeys": authorityKeys };
    return request.get("/org/manager/judgeAuthorityArray", { params: params });
}