import request from '@/utils/request'
import URL_KEY from '@/api/urlKeys'

const menuService = "/org/menu/";//菜单service

export const menu_queryPageApi = menuService + URL_KEY.page;//菜单分页查询
export const menu_queryListApi = menuService + URL_KEY.list;//菜单全部查询
export const menu_queryByIdApi = menuService + URL_KEY.id;//菜单id查询
export const menu_insertApi = menuService + URL_KEY.add;//菜单新增
export const menu_updateApi = menuService + URL_KEY.update;//菜单修改
export const menu_deleteApi = menuService + URL_KEY.delete;//菜单删除
export const menu_deletesApi = menuService + URL_KEY.deletes;//菜单批量删除
export const menu_queryTreeApi = menuService + "menuTree";//菜单树查询

/**
*@description 新增
*@param menu
*@author light_dust_creator
*@date 2019-12-16 12:02:29
*/
export function menuInsert(menu) {
	return request.post(menu_insertApi, menu);
}

/**
*@description 删除
*@param menu
*@author light_dust_creator
*@date 2019-12-16 12:02:29
*/
export function menuDelete(menu) {
	return request.post(menu_deleteApi, menu);
}

/**
*@description 删除_批量
*@param menu
*@author light_dust_creator
*@date 2019-12-16 12:02:29
*/
export function menuDeletes(menu) {
	return request.post(menu_deletesApi, menu);
}

/**
*@description 修改
*@param menu
*@author light_dust_creator
*@date 2019-12-16 12:02:29
*/
export function menuUpdate(menu) {
	return request.post(menu_updateApi, menu);
}

/**
*@description 查询分页
*@param queryParam
*@author light_dust_creator
*@date 2019-12-16 12:02:29
*/
export function menuPage(queryParam) {
	return request.get(menu_queryPageApi, { params: queryParam });
}

/**
*@description 查询所有
*@param queryParam
*@author light_dust_creator
*@date 2019-12-16 12:02:29
*/
export function menuList(queryParam) {
	return request.get(menu_queryListApi, { params: queryParam });
}

/**
*@description 查询详情
*@param menuId id
*@author light_dust_creator
*@date 2019-12-16 12:02:29
*/
export function getMenu(menuId) {
	return request.get(menu_queryByIdApi + menuId);
}

/**
*@description 得到菜单树
*@param queryParam
*@author light_dust
*@date 2019-12-20
*/
export function menuTree(queryParam) {
	return request.get(menu_queryTreeApi, { params: queryParam });
}