import request from '@/utils/request'
import URL_KEY from '@/api/urlKeys'

const unitService = "/org/unit/";//角色service

export const unitListUrl = "/org/orgAuthority/unitList";//用户列表页面
export const unitFormUrl = "/org/orgAuthority/unitForm";//用户详情页面

export const unit_queryPageApi = unitService + URL_KEY.page;//用户分页查询
export const unit_queryListApi = unitService + URL_KEY.list;//用户全部查询
export const unit_queryByIdApi = unitService + URL_KEY.id;//用户id查询
export const unit_insertApi = unitService + URL_KEY.add;//用户新增
export const unit_updateApi = unitService + URL_KEY.update;//用户修改
export const unit_deleteApi = unitService + URL_KEY.delete;//用户删除
export const unit_deletesApi = unitService + URL_KEY.deletes;//用户批量删除

/**
*@description 新增
*@param unit
*@author light_dust_creator
*@date 2019-12-26 11:34:54
*/
export function unitInsert(unit) {
	return request.post(unit_insertApi, unit);
}

/**
*@description 删除
*@param unit
*@author light_dust_creator
*@date 2019-12-26 11:34:54
*/
export function unitDelete(unit) {
	return request.post(unit_deleteApi, unit);
}

/**
*@description 删除_批量
*@param unit
*@author light_dust_creator
*@date 2019-12-26 11:34:54
*/
export function unitDeletes(unit) {
	return request.post(unit_deletesApi, unit);
}

/**
*@description 修改
*@param unit
*@author light_dust_creator
*@date 2019-12-26 11:34:54
*/
export function unitUpdate(unit) {
	return request.post(unit_updateApi, unit);
}

/**
*@description 查询分页
*@param queryParam
*@author light_dust_creator
*@date 2019-12-26 11:34:54
*/
export function unitPage(queryParam) {
	return request.get(unit_queryPageApi, { params: queryParam });
}

/**
*@description 查询所有
*@param queryParam
*@author light_dust_creator
*@date 2019-12-26 11:34:54
*/
export function unitList(queryParam) {
	return request.get(unit_queryListApi, { params: queryParam });
}

/**
*@description 查询详情
*@param unitId id
*@author light_dust_creator
*@date 2019-12-26 11:34:54
*/
export function getUnit(unitId) {
	return request.get(unit_queryByIdApi + unitId);
}

