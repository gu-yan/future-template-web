import request from '@/utils/request'
import URL_KEY from '@/api/urlKeys'

const moduleService = "/org/module/";//模块配置service

export const module_queryPageApi = moduleService + URL_KEY.page;//模块配置分页查询
export const module_queryListApi = moduleService + URL_KEY.list;//模块配置全部查询
export const module_queryByIdApi = moduleService + URL_KEY.id;//模块配置id查询
export const module_insertApi = moduleService + URL_KEY.add;//模块配置新增
export const module_updateApi = moduleService + URL_KEY.update;//模块配置修改
export const module_deleteApi = moduleService + URL_KEY.delete;//模块配置删除
export const module_deletesApi = moduleService + URL_KEY.deletes;//模块配置批量删除
/**
 *@description 模块配置新增
 *@param module  
 *@author light_dust_creator
 *@date 2019-09-18 13:38:27
 */
export function moduleInsert(module) {
    return request.post(module_insertApi, module);
}

/**
 *@description 模块配置删除
 *@param module  
 *@author light_dust_creator
 *@date 2019-09-18 13:38:27
 */
export function moduleDelete(module) {
    return request.post(module_deleteApi, module);
}

/**
 *@description 模块配置删除
 *@param module  
 *@author light_dust_creator
 *@date 2019-09-18 13:38:27
 */
export function moduleDeletes(module) {
    return request.post(module_deletesApi, module);
}

/**
*@description 模块配置修改
*@param module  
*@author light_dust_creator
*@date 2019-09-18 13:38:27
*/
export function moduleUpdate(module) {
    return request.post(module_updateApi, module);
}

/**
*@description 模块配置查询分页
*@param queryParam  
*@author light_dust_creator
*@date 2019-09-18 13:38:27
*/
export function modulePage(queryParam) {
    return request.get(module_queryPageApi, { params: queryParam });
}

/**
*@description 模块配置查询所有
*@param queryParam 
*@author light_dust_creator
*@date 2019-09-18 13:38:27
*/
export function moduleList(queryParam) {
    return request.get(module_queryListApi, { params: queryParam });
}

/**
*@description 模块配置查询详情
*@param moduleId 模块配置id
*@author light_dust_creator
*@date 2019-09-18 13:38:27
*/
export function getCronTask(moduleId) {
    return request.get(module_queryByIdApi + moduleId);
}
