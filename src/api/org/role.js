import request from '@/utils/request'
import URL_KEY from '@/api/urlKeys'

const roleService = "/org/role/";//角色service

export const roleListUrl = "/org/orgAuthority/roleList"//角色列表页面
export const roleFormUrl = "/org/orgAuthority/roleForm"//角色详情页面

export const role_queryPageApi = roleService + URL_KEY.page;//用户分页查询
export const role_queryListApi = roleService + URL_KEY.list;//用户全部查询
export const role_queryByIdApi = roleService + URL_KEY.id;//用户id查询
export const role_insertApi = roleService + URL_KEY.add;//用户新增
export const role_updateApi = roleService + URL_KEY.update;//用户修改
export const role_deleteApi = roleService + URL_KEY.delete;//用户删除
export const role_deletesApi = roleService + URL_KEY.deletes;//用户批量删除
export const role_menuTreeApi = roleService + "getRoleMenuTree";//角色菜单树
export const role_updateRoleResourceAuthorityApi = roleService + "updateRoleResourceAuthority";//角色菜单树

/**
*@description 新增
*@param role
*@author light_dust_creator
*@date 2019-12-30 19:33:49
*/
export function roleInsert(role) {
	return request.post(role_insertApi, role);
}

/**
*@description 删除
*@param role
*@author light_dust_creator
*@date 2019-12-30 19:33:49
*/
export function roleDelete(role) {
	return request.post(role_deleteApi, role);
}

/**
*@description 删除_批量
*@param role
*@author light_dust_creator
*@date 2019-12-30 19:33:49
*/
export function roleDeletes(role) {
	return request.post(role_deletesApi, role);
}

/**
*@description 修改
*@param role
*@author light_dust_creator
*@date 2019-12-30 19:33:49
*/
export function roleUpdate(role) {
	return request.post(role_updateApi, role);
}

/**
*@description 查询分页
*@param queryParam
*@author light_dust_creator
*@date 2019-12-30 19:33:49
*/
export function rolePage(queryParam) {
	return request.get(role_queryPageApi, { params: queryParam });
}

/**
*@description 查询所有
*@param queryParam
*@author light_dust_creator
*@date 2019-12-30 19:33:49
*/
export function roleList(queryParam) {
	return request.get(role_queryListApi, { params: queryParam });
}

/**
*@description 查询详情
*@param roleId id
*@author light_dust_creator
*@date 2019-12-30 19:33:49
*/
export function getRole(roleId) {
	return request.get(role_queryByIdApi + roleId);
}

/**
*@description 查询角色菜单树
*@param queryParam id
*@author light_dust_creator
*@date 2019-12-30 19:33:49
*/
export function getRoleMenuTree(queryParam) {
	return request.get(role_menuTreeApi, { params: queryParam });
}

/**
*@description 更新角色权限
*@param queryParam id
*@author light_dust_creator
*@date 2019-12-30 19:33:49
*/
export function updateRoleResourceAuthority(queryParam) {
	return request.post(role_updateRoleResourceAuthorityApi, queryParam);
}



