/*
 * @Description: 
 * @Autor: Zhu_liangyu
 * @Date: 2020-05-31 17:28:38
 */
import request from '@/utils/request'
import URL_KEY from '@/api/urlKeys'

const anonymousService = "/org/anonymous/";

export const anonymousListUrl = "/org/anonymous/list";
export const anonymousFormUrl = "/org/anonymous/form";

export const anonymous_queryPageApi = anonymousService + URL_KEY.page;
export const anonymous_queryListApi = anonymousService + URL_KEY.list;
export const anonymous_queryByIdApi = anonymousService + URL_KEY.id;
export const anonymous_insertApi = anonymousService + URL_KEY.add;
export const anonymous_updateApi = anonymousService + URL_KEY.update;
export const anonymous_deleteApi = anonymousService + URL_KEY.delete;
export const anonymous_deletesApi = anonymousService + URL_KEY.deletes;

/**
*@description 新增
*@param anonymous
*@author light_dust_creator
*@date 2020-07-05 18:15:49
*/
export function anonymousInsert(anonymous) {
	return request.post(anonymous_insertApi, anonymous);
}

/**
*@description 删除
*@param anonymous
*@author light_dust_creator
*@date 2020-07-05 18:15:49
*/
export function anonymousDelete(anonymous) {
	return request.post(anonymous_deleteApi, anonymous);
}

/**
*@description 删除_批量
*@param anonymous
*@author light_dust_creator
*@date 2020-07-05 18:15:49
*/
export function anonymousDeletes(anonymouss) {
	return request.post(anonymous_deletesApi, anonymouss);
}

/**
*@description 修改
*@param anonymous
*@author light_dust_creator
*@date 2020-07-05 18:15:49
*/
export function anonymousUpdate(anonymous) {
	return request.post(anonymous_updateApi, anonymous);
}

/**
*@description 查询分页
*@param queryParam
*@author light_dust_creator
*@date 2020-07-05 18:15:49
*/
export function anonymousPage(queryParam) {
	return request.get(anonymous_queryPageApi, { params: queryParam });
}

/**
*@description 查询所有
*@param queryParam
*@author light_dust_creator
*@date 2020-07-05 18:15:49
*/
export function anonymousList(queryParam) {
	return request.get(anonymous_queryListApi, { params: queryParam });
}

/**
*@description 查询详情
*@param anonymousId id
*@author light_dust_creator
*@date 2020-07-05 18:15:49
*/
export function getAnonymous(anonymousId) {
	return request.get(anonymous_queryByIdApi + anonymousId);
}