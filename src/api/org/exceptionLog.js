/*
 * @Description: 
 * @Autor: Zhu_liangyu
 * @Date: 2020-07-13 16:57:52
 */
import request from '@/utils/request'
import URL_KEY from '@/api/urlKeys'

const exceptionLogService = "/org/exceptionLog/";

export const exceptionLogListUrl = "/org/exceptionLog/lsit";
export const exceptionLogFormUrl = "/org/exceptionLog/form";

export const exceptionLog_queryPageApi = exceptionLogService + URL_KEY.page;
export const exceptionLog_queryListApi = exceptionLogService + URL_KEY.list;
export const exceptionLog_queryByIdApi = exceptionLogService + URL_KEY.id;
export const exceptionLog_insertApi = exceptionLogService + URL_KEY.add;
export const exceptionLog_updateApi = exceptionLogService + URL_KEY.update;
export const exceptionLog_deleteApi = exceptionLogService + URL_KEY.delete;
export const exceptionLog_deletesApi = exceptionLogService + URL_KEY.deletes;

/**
*@description 新增
*@param exceptionLog
*@author light_dust_creator
*@date 2020-07-13 16:57:52
*/
export function exceptionLogInsert(exceptionLog) {
	return request.post(exceptionLog_insertApi, exceptionLog);
}

/**
*@description 删除
*@param exceptionLog
*@author light_dust_creator
*@date 2020-07-13 16:57:52
*/
export function exceptionLogDelete(exceptionLog) {
	return request.post(exceptionLog_deleteApi, exceptionLog);
}

/**
*@description 删除_批量
*@param exceptionLog
*@author light_dust_creator
*@date 2020-07-13 16:57:52
*/
export function exceptionLogDeletes(exceptionLogs) {
	return request.post(exceptionLog_deletesApi, exceptionLogs);
}

/**
*@description 修改
*@param exceptionLog
*@author light_dust_creator
*@date 2020-07-13 16:57:52
*/
export function exceptionLogUpdate(exceptionLog) {
	return request.post(exceptionLog_updateApi, exceptionLog);
}

/**
*@description 查询分页
*@param queryParam
*@author light_dust_creator
*@date 2020-07-13 16:57:52
*/
export function exceptionLogPage(queryParam) {
	return request.get(exceptionLog_queryPageApi, { params: queryParam });
}

/**
*@description 查询所有
*@param queryParam
*@author light_dust_creator
*@date 2020-07-13 16:57:52
*/
export function exceptionLogList(queryParam) {
	return request.get(exceptionLog_queryListApi, { params: queryParam });
}

/**
*@description 查询详情
*@param exceptionLogId id
*@author light_dust_creator
*@date 2020-07-13 16:57:52
*/
export function getExceptionLog(exceptionLogId) {
	return request.get(exceptionLog_queryByIdApi + exceptionLogId);
}