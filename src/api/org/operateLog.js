/*
 * @Description: 
 * @Autor: Zhu_liangyu
 * @Date: 2020-07-13 16:59:46
 */
import request from '@/utils/request'
import URL_KEY from '@/api/urlKeys'

const operateLogService = "/org/operateLog/";

export const operateLogListUrl = "/org/operateLog/list";
export const operateLogFormUrl = "/org/operateLog/form";

export const operateLog_queryPageApi = operateLogService + URL_KEY.page;
export const operateLog_queryListApi = operateLogService + URL_KEY.list;
export const operateLog_queryByIdApi = operateLogService + URL_KEY.id;
export const operateLog_insertApi = operateLogService + URL_KEY.add;
export const operateLog_updateApi = operateLogService + URL_KEY.update;
export const operateLog_deleteApi = operateLogService + URL_KEY.delete;
export const operateLog_deletesApi = operateLogService + URL_KEY.deletes;

/**
*@description 新增
*@param operateLog
*@author light_dust_creator
*@date 2020-07-13 16:59:46
*/
export function operateLogInsert(operateLog) {
	return request.post(operateLog_insertApi, operateLog);
}

/**
*@description 删除
*@param operateLog
*@author light_dust_creator
*@date 2020-07-13 16:59:46
*/
export function operateLogDelete(operateLog) {
	return request.post(operateLog_deleteApi, operateLog);
}

/**
*@description 删除_批量
*@param operateLog
*@author light_dust_creator
*@date 2020-07-13 16:59:46
*/
export function operateLogDeletes(operateLogs) {
	return request.post(operateLog_deletesApi, operateLogs);
}

/**
*@description 修改
*@param operateLog
*@author light_dust_creator
*@date 2020-07-13 16:59:46
*/
export function operateLogUpdate(operateLog) {
	return request.post(operateLog_updateApi, operateLog);
}

/**
*@description 查询分页
*@param queryParam
*@author light_dust_creator
*@date 2020-07-13 16:59:46
*/
export function operateLogPage(queryParam) {
	return request.get(operateLog_queryPageApi, { params: queryParam });
}

/**
*@description 查询所有
*@param queryParam
*@author light_dust_creator
*@date 2020-07-13 16:59:46
*/
export function operateLogList(queryParam) {
	return request.get(operateLog_queryListApi, { params: queryParam });
}

/**
*@description 查询详情
*@param operateLogId id
*@author light_dust_creator
*@date 2020-07-13 16:59:46
*/
export function getOperateLog(operateLogId) {
	return request.get(operateLog_queryByIdApi + operateLogId);
}