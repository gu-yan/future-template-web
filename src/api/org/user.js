import request from '@/utils/request'
import { objectToUrlParams } from '@/utils/tool-str'
import URL_KEY from '@/api/urlKeys'

const userService = "/org/user/";//用户service

export const userListUrl = "/org/orgAuthority/userList";//用户列表页面
export const userFormUrl = "/org/orgAuthority/userForm";//用户详情页面

export const user_queryPageApi = userService + URL_KEY.page;//用户分页查询
export const user_queryListApi = userService + URL_KEY.list;//用户全部查询
export const user_queryByIdApi = userService + URL_KEY.id;//用户id查询
export const user_insertApi = userService + URL_KEY.add;//用户新增
export const user_updateApi = userService + URL_KEY.update;//用户修改
export const user_deleteApi = userService + URL_KEY.delete;//用户删除
export const user_deletesApi = userService + URL_KEY.deletes;//用户批量删除
export const user_infoApi = userService + "info"//用户导出功能
export const user_exportApi = userService + "export"//用户导出功能
export const user_importApi = userService + "importExcel"//用户导入功能
export const user_restPass = userService + "restPass"//用户密码重置

//用户登录
export function login(data) {
    var url = "/org/login"
    return request.post(url, data);
}

//用户注销
export function logout() {
    return request.post('/org/login/logout');
}

//得到用户信息(ehcache缓存)
export function getInfo(token) {
    return request.get(user_infoApi, { params: { token } });
}

//用户新增
export function userInsert(user, extData) {
    var url = user_insertApi;
    url += objectToUrlParams(extData, true);
    return request.post(url, user);
}

//用户删除
export function userDelete(user) {
    return request.post(user_deleteApi, user);
}

//用户删除_批量
export function userDeletes(users) {
    return request.post(user_deletesApi, users);
}

//用户分页
export function userPage(queryParam) {
    return request.get(user_queryPageApi, { params: queryParam });
}

//用户修改
export function userUpdate(user) {
    return request.post(user_updateApi, user);
}

//得到用户信息
export function getUser(userId) {
    return request.get(user_queryByIdApi + userId);
}

//用户密码重置
export function restPass(user) {
    return request.post(user_restPass, user);
}

//导出用户信息
export function exportUser(queryParam) {
    return request.post(user_exportApi, queryParam, { responseType: 'blob' });
}
//导出用户信息
export function importUser() {
    return request.get(user_importApi);
}

