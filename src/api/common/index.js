import request from '@/utils/request'
export const uploadApi = "/future/org/file/upload"//文件上传api
export const uploadImgApi = "/future/org/file/uploadImg"//上传图片api
export const viewImgApi = "/future/org/file/downloadFile?fileId="//查看图片api

export function listDictionary(codeItemId) {
    return request.get('/cms/common/listDictionary/' + codeItemId);
}