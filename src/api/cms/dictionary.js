import request from '@/utils/request'
import URL_KEY from '@/api/urlKeys'

const dictionaryService = "/cms/dictionary/";//数据字典service

export const dictionary_queryPageApi = dictionaryService + URL_KEY.page;//数据字典分页查询
export const dictionary_queryListApi = dictionaryService + URL_KEY.list;//数据字典全部查询
export const dictionary_queryByIdApi = dictionaryService + URL_KEY.id;//数据字典id查询
export const dictionary_insertApi = dictionaryService + URL_KEY.add;//数据字典新增
export const dictionary_updateApi = dictionaryService + URL_KEY.update;//数据字典修改
export const dictionary_deleteApi = dictionaryService + URL_KEY.delete;//数据字典删除
export const dictionary_deletesApi = dictionaryService + URL_KEY.deletes;//数据字典批量删除

/**
 *@description 数据字典新增
 *@param dictionary  
 *@author light_dust_creator
 *@date 2019-09-18 13:38:27
 */
export function dictionaryInsert(dictionary) {
    return request.post(dictionary_insertApi, dictionary);
}

/**
 *@description 数据字典删除
 *@param dictionary  
 *@author light_dust_creator
 *@date 2019-09-18 13:38:27
 */
export function dictionaryDelete(dictionary) {
    return request.post(dictionary_deleteApi, dictionary);
}

/**
 *@description 数据字典删除
 *@param dictionary  
 *@author light_dust_creator
 *@date 2019-09-18 13:38:27
 */
export function dictionaryDeletes(dictionary) {
    return request.post(dictionary_deletesApi, dictionary);
}

/**
*@description 数据字典修改
*@param dictionary  
*@author light_dust_creator
*@date 2019-09-18 13:38:27
*/
export function dictionaryUpdate(dictionary) {
    return request.post(dictionary_updateApi, dictionary);
}

/**
*@description 数据字典查询分页
*@param queryParam  
*@author light_dust_creator
*@date 2019-09-18 13:38:27
*/
export function dictionaryPage(queryParam) {
    return request.get(dictionary_queryPageApi, { params: queryParam });
}

/**
*@description 数据字典查询所有
*@param queryParam 
*@author light_dust_creator
*@date 2019-09-18 13:38:27
*/
export function dictionaryList(queryParam) {
    return request.get(dictionary_queryListApi, { params: queryParam });
}

/**
*@description 数据字典查询详情
*@param dictionaryId 数据字典id
*@author light_dust_creator
*@date 2019-09-18 13:38:27
*/
export function getDictionary(dictionaryId) {
    return request.get(dictionary_queryByIdApi + dictionaryId);
}