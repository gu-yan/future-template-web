import request from '@/utils/request'
import URL_KEY from '@/api/urlKeys'

const cronTaskService = "/cms/cronTask/";//定时任务service

export const cronTask_queryPageApi = cronTaskService + URL_KEY.page;//定时任务分页查询
export const cronTask_queryListApi = cronTaskService + URL_KEY.list;//定时任务全部查询
export const cronTask_queryByIdApi = cronTaskService + URL_KEY.id;//定时任务id查询
export const cronTask_insertApi = cronTaskService + URL_KEY.add;//定时任务新增
export const cronTask_updateApi = cronTaskService + URL_KEY.update;//定时任务修改
export const cronTask_deleteApi = cronTaskService + URL_KEY.delete;//定时任务删除
export const cronTask_deletesApi = cronTaskService + URL_KEY.deletes;//定时任务批量删除
export const cronTask_changeActiveApi = cronTaskService + "changeActive";//修改定时任务状态
export const cronTask_runOnceJobApi = cronTaskService + "runOnceJob";//执行一次定时任务

/**
 *@description 定时任务新增
 *@param cronTask  
 *@author light_dust_creator
 *@date 2019-09-18 13:38:27
 */
export function cronTaskInsert(cronTask) {
    return request.post(cronTask_insertApi, cronTask);
}

/**
 *@description 定时任务删除
 *@param cronTask  
 *@author light_dust_creator
 *@date 2019-09-18 13:38:27
 */
export function cronTaskDelete(cronTask) {
    return request.post(cronTask_deleteApi, cronTask);
}

/**
 *@description 定时任务删除
 *@param cronTask  
 *@author light_dust_creator
 *@date 2019-09-18 13:38:27
 */
export function cronTaskDeletes(cronTask) {
    return request.post(cronTask_deletesApi, cronTask);
}

/**
*@description 定时任务修改
*@param cronTask  
*@author light_dust_creator
*@date 2019-09-18 13:38:27
*/
export function cronTaskUpdate(cronTask) {
    return request.post(cronTask_updateApi, cronTask);
}

/**
*@description 定时任务查询分页
*@param queryParam  
*@author light_dust_creator
*@date 2019-09-18 13:38:27
*/
export function cronTaskPage(queryParam) {
    return request.get(cronTask_queryPageApi, { params: queryParam });
}

/**
*@description 定时任务查询所有
*@param queryParam 
*@author light_dust_creator
*@date 2019-09-18 13:38:27
*/
export function cronTaskList(queryParam) {
    return request.get(cronTask_queryListApi, { params: queryParam });
}

/**
*@description 定时任务查询详情
*@param cronTaskId 定时任务id
*@author light_dust_creator
*@date 2019-09-18 13:38:27
*/
export function getCronTask(cronTaskId) {
    return request.get(cronTask_queryByIdApi + cronTaskId);
}

/**
*@description 修改定时任务状态
*@param queryParam
*@author light_dust_creator
*@date 2019-09-18 13:38:27
*/
export function changeActive(queryParam) {
    return request.get(cronTask_changeActiveApi, { params: queryParam });
}

/**
*@description 执行一次定时任务
*@param queryParam
*@author light_dust_creator
*@date 2019-09-18 13:38:27
*/
export function runOnceJob(queryParam) {
    return request.get(cronTask_runOnceJobApi, { params: queryParam });
}