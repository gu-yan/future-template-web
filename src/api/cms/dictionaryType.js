import request from '@/utils/request'
import URL_KEY from '@/api/urlKeys'

const dictionaryTypeService = "/cms/dictionaryType/";//数据类型字典service

export const dictionaryTypeListUrl = "/cms/dictionaryType/list";//数据字典类型列表页面
export const dictionaryTypeFormUrl = "/cms/dictionaryType/form";//数据字典类型详情页面

export const dictionaryType_queryPageApi = dictionaryTypeService + URL_KEY.page;//数据字典类型分页查询
export const dictionaryType_queryListApi = dictionaryTypeService + URL_KEY.list;//数据字典类型全部查询
export const dictionaryType_queryByIdApi = dictionaryTypeService + URL_KEY.id;//数据字典类型id查询
export const dictionaryType_insertApi = dictionaryTypeService + URL_KEY.add;//数据字典类型新增
export const dictionaryType_updateApi = dictionaryTypeService + URL_KEY.update;//数据字典类型修改
export const dictionaryType_deleteApi = dictionaryTypeService + URL_KEY.delete;//数据字典类型删除
export const dictionaryType_deletesApi = dictionaryTypeService + URL_KEY.deletes;//数据字典类型批量删除

/**
 *@description 字典类型新增
 *@param dictionaryType  
 *@author light_dust_creator
 *@date 2019-09-18 13:14:51
 */
export function dictionaryTypeInsert(dictionaryType) {
    return request.post(dictionaryType_insertApi, dictionaryType);
}

/**
 *@description 字典类型删除
 *@param dictionaryType  
 *@author light_dust_creator
 *@date 2019-09-18 13:14:51
 */
export function dictionaryTypeDelete(dictionaryType) {
    return request.post(dictionaryType_deleteApi, dictionaryType);
}

/**
 *@description 字典类型删除
 *@param dictionaryType  
 *@author light_dust_creator
 *@date 2019-09-18 13:14:51
 */
export function dictionaryTypeDeletes(dictionaryType) {
    return request.post(dictionaryType_deletesApi, dictionaryType);
}

/**
*@description 字典类型修改
*@param dictionaryType  
*@author light_dust_creator
*@date 2019-09-18 13:14:51
*/
export function dictionaryTypeUpdate(dictionaryType) {
    return request.post(dictionaryType_updateApi, dictionaryType);
}

/**
*@description 字典类型查询分页
*@param queryParam  
*@author light_dust_creator
*@date 2019-09-18 13:14:51
*/
export function dictionaryTypePage(queryParam) {
    return request.get(dictionaryType_queryPageApi, { params: queryParam });
}

/**
*@description 字典类型查询所有
*@param queryParam 
*@author light_dust_creator
*@date 2019-09-18 13:14:51
*/
export function dictionaryTypeList(queryParam) {
    return request.get(dictionaryType_queryListApi, { params: queryParam });
}

/**
*@description 字典类型查询详情
*@param dictionaryTypeId 字典类型id
*@author light_dust_creator
*@date 2019-09-18 13:14:51
*/
export function getDictionaryType(dictionaryTypeId) {
    return request.get(dictionaryType_queryByIdApi + dictionaryTypeId);
}