const URL_KEY = {
    page: 'page',
    list: 'list',
    id: 'id/',
    add: 'add',
    addBatch: 'addBatch',
    update: 'update',
    updateBatch: 'updateBatch',
    delete: 'delete',
    deletes: "deletes"
}

export default URL_KEY;

