/*
 * @Description: 
 * @Autor: Zhu_liangyu
 * @Date: 2020-05-09 10:52:56
 */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Antd from 'ant-design-vue'
import '@/styles/cartoon.css' // 首屏白屏加载缓慢css
import '@/styles/index.scss' // global css
import 'ant-design-vue/dist/antd.css';
import '@/permission'
Vue.use(Antd);
import "@/core/use"
import init from "@/core/init"

Vue.config.productionTip = false

new Vue({
  router,
  store,
  created: init,
  render: h => h(App)
}).$mount('#app')
