/*
 * @Description: 
 * @Autor: Zhu_liangyu
 * @Date: 2020-05-09 10:52:56
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import { BasicLayout } from '@/layouts'

Vue.use(VueRouter)

const routes = [
  {
    path: '/403',
    name: '403',
    component: () => import('@/views/error/403'),
    hidden: true
  },
  {
    path: '/404',
    name: '404',
    component: () => import('@/views/error/404'),
    hidden: true
  },
  {
    path: '/500',
    name: '500',
    component: () => import('@/views/error/500'),
    hidden: true
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login'),
    hidden: true
  },
  {
    path: "/",
    name: "index",
    meta: {
      title: "首页",
      icon: "icon-yibiaopan"
    },
    component: BasicLayout,
    redirect: "/dashboard"
  },
  {
    path: '/redirect',
    component: BasicLayout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/profile/Redirect')
      }
    ]
  }
]

// 重写路由的push方法
const routerPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error => error)
}

const createRouter = () => new VueRouter({
  mode: 'history',
  // base: process.env.BASE_URL,
  scrollBehavior: () => ({ y: 0 }),
  routes
})

const router = createRouter();

export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher // reset router
}

export default router
