export const ACCESS_TOKEN = 'Access-Token'//tokenKey
export const COLLAPSED = 'COLLAPSED'//是否收缩左侧栏
export const DEFAULT_MENU_THEME = 'DEFAULT_MENU_THEME'//菜单主题
export const DEFAULT_MENU_TYPE = 'DEFAULT_MENU_TYPE'//菜单类型
export const DEFAULT_FIXED_HEADER = 'DEFAULT_FIXED_HEADER'//固定顶部
export const DEFAULT_FIXED_SIDEMENU = 'DEFAULT_FIXED_SIDEMENU'//固定左侧菜单栏
