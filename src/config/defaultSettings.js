export default {
    menuTheme: 'dark', // theme for nav menu
    menuType: 'sideMenu', // nav menu position: sidemenu or topmenu
    fixHeader: true, // sticky header
    fixSiderbar: true, // sticky siderbar
    // vue-ls options
    storageOptions: {
        namespace: 'future__', // key prefix
        name: 'ls', // name variable Vue.[ls] or this.[$ls],
        storage: 'local' // storage name session, local, memory
    }
}